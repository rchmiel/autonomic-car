#include <Servo.h> //Biblioteka odpowiedzialna za serwa

Servo serwomechanizm;  //Tworzymy obiekt, dzięki któremu możemy odwołać się do serwa
Servo serwomechanizm2;  //Tworzymy obiekt, dzięki któremu możemy odwołać się do serwa

#define trigPin 12
#define echoPin 11

#define leftSensor 2
#define rightSensor 10


int obstaceForLeftWheel;
int obstaceForRightWheel;


// connect motor controller pins to Arduino digital pins
// motor one
int enA = 3;
int in1 = 9;
int in2 = 8;
// motor two
int enB = 5;
int in3 = 7;
int in4 = 6;

// set speed of possible range 0~255
int speed = 220;

int position = 1;

void setup() {
  Serial.begin(9600);


  serwomechanizm.attach(4);  //Serwomechanizm podłączony do pinu 9
  serwomechanizm2.attach(13);  //Serwomechanizm2 podłączony do pinu 9

  pinMode(trigPin, OUTPUT); //Pin, do którego podłączymy trig jako wyjście
  pinMode(echoPin, INPUT); //a echo, jako wejście

  pinMode(leftSensor, INPUT); //Pin, do którego podłączymy trig jako wyjście
  pinMode(rightSensor, INPUT); //Pin, do którego podłączymy trig jako wyjście

  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  serwomechanizm.write(90); //Wykonaj ruch
  serwomechanizm2.write(90); //Wykonaj ruch
}

void loop() {
  findObstacle();

  Serial.print(zmierzOdleglosc());
  Serial.println(" cm");

  if (zmierzOdleglosc() < 42) {
    speed = 110;
  } else {
    speed = 180;
  }


  if (zmierzOdleglosc() > 16) {
    if (obstaceForLeftWheel == 1 || obstaceForRightWheel == 1) {
      Serial.println("Obstacle!");
      if (obstaceForRightWheel == 1) {
        returnByLeft();
      } else if (obstaceForLeftWheel == 1) {
        returnByRight();
      }
    } else {
      goForward();
    }

  } else {
    if (random(0, 1) == 0) {
      returnByRight();
    }else {
      returnByLeft();
    }

  }

  lookAround(position);
  if (position > 4) {
    position = 1;
  } else {
    position++;
  }
}



void lookAround(int index) {
  int hight = 105;
  switch (index) {
    case 1:
      serwomechanizm.write(75); //Wykonaj ruch
      serwomechanizm2.write(hight); //Wykonaj ruch
      delay(170);
      break;
    case 2:
      serwomechanizm.write(90); //Wykonaj ruch
      serwomechanizm2.write(hight); //Wykonaj ruch
      delay(170);
      break;
    case 3:
      serwomechanizm.write(105); //Wykonaj ruch
      serwomechanizm2.write(hight); //Wykonaj ruch
      delay(170);
      break;
    case 4:
      serwomechanizm.write(90); //Wykonaj ruch
      serwomechanizm2.write(hight); //Wykonaj ruch
      delay(170);
      break;
  }
}

boolean findObstacle() {

  obstaceForLeftWheel = !digitalRead(leftSensor);
  obstaceForRightWheel = !digitalRead(rightSensor);

  Serial.print("left sensor: ");
  Serial.println(obstaceForLeftWheel);
  Serial.print("right sensor: ");
  Serial.println(obstaceForRightWheel);
}

int zmierzOdleglosc() {
  long signalTime, distance;

  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  signalTime = pulseIn(echoPin, HIGH);
  distance = signalTime / 58;

  return distance;
}

void goForward()
{
  Serial.println("goForward");

  // this function will run the motors in both directions at a fixed speed
  // turn on motor A
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  // set speed to 150 out of possible range 0~255
  //  analogWrite(enA, speed);/
  // turn on motor B
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  // set speed to 150 out of possible range 0~255
  //  analogWrite(enB, speed);/
  analogWrite(enA, speed);
  analogWrite(enB, speed);
  delay(100);
}

void returnByRight()
{
  int returnSpeed = 200;
  stopEngine();
  delay(250);

  goBackward();
  delay(150);

  stopEngine();
  delay(250);

  Serial.println("Return by right");
  // this function will run the motors in both directions at a fixed speed
  // turn on motor A
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  // set speed to 150 out of possible range 0~255
  //  analogWrite(enA, speed);/
  // turn on motor B
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  // set speed of possible range 0~255
  analogWrite(enA, returnSpeed);
  analogWrite(enB, returnSpeed);
  delay(random(450, 650));
  stopEngine();
  delay(250);
}

void returnByLeft()
{
  int returnSpeed = 200;
  stopEngine();
  delay(250);

  goBackward();
  delay(150);

  stopEngine();
  delay(250);

  Serial.println("Return by left");
  // this function will run the motors in both directions at a fixed speed
  // turn on motor A
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  // set speed to 150 out of possible range 0~255
  //  analogWrite(enA, speed);/
  // turn on motor B
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  // set speed of possible range 0~255
  analogWrite(enA, returnSpeed);
  analogWrite(enB, returnSpeed);
  delay(random(450, 650));
  stopEngine();
  delay(250);
}

void goBackward()
{
  Serial.println("goBackward");

  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  analogWrite(enA, 200);
  analogWrite(enB, 200);
}

void goLeft() {
  Serial.println("goLeft");
}

void goRight() {
  Serial.println("goRight");
}

void stopEngine() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}

